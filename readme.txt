/**
 * PHP Login and User Management.
 *
 * LICENSE:
 *
 * This source file is subject to the licensing terms that
 * is available through the world-wide-web at the following URI:
 * http://codecanyon.net/wiki/support/legal-terms/licensing-terms/.
 *
 * @author       Jigowatt <info@jigowatt.co.uk>
 * @author       Matt Gates <info@mgates.me>
 * @copyright    Copyright (c) 2009-2015 Jigowatt Ltd.
 * @license      http://codecanyon.net/wiki/support/legal-terms/licensing-terms/
 * @link         http://codecanyon.net/item/php-login-user-management/49008
 */

/**
 *
 * Description
 *
 */

Thank you for purchasing PHP Login and User Management, the MySQL powered website login script, another high quality script designed and created by Jigowatt exclusively for Envato Marketplaces.

By integrating this script into your existing website you can create a private user area where visitors to your website have to be logged in to view page content.

It features manageable User Levels via the secure Control Panel for different levels of page security.

Please note, this script can be used as a standalone product, however; we recommend you use this script as a base and customise it to suit your individual needs. Importantly, we cannot support 3rd party modification.

/**
 *
 * Documentation
 *
 */

http://envato.jigowatt.co.uk/demos/phplogin/install.php


/**
 *
 * Credits
 *
 */

/* CSS */
Twitter Bootstrap:
http://twitter.github.com/bootstrap/

/* Images */
Social Buttons:
http://www.mfcreative.co.uk/socialbuttonspsd/
http://www.elegantthemes.com/blog/resources/free-social-media-icon-set

/* jQuery Plugins */
HTML5 Placeholder (for IE support):
https://github.com/mathiasbynens/jquery-placeholder

Flot:
http://code.google.com/p/flot/

Haschange
http://benalman.com/projects/jquery-hashchange-plugin/

Validation
http://bassistance.de/jquery-plugins/jquery-plugin-validation/

Bootstrap Datepicker:
https://github.com/eternicode/bootstrap-datepicker

Select2:
http://ivaynberg.github.com/select2/